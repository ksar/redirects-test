<?php
namespace frontend\exceptions;

class DuplicateClickException extends AbstractException
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Duplicate Click';
    }
}
