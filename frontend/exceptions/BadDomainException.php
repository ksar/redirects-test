<?php
namespace frontend\exceptions;

class BadDomainException extends AbstractException
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Bad Domain';
    }
}
