<?php
namespace frontend\exceptions;

use frontend\models\Click;
use yii\base\Exception as BaseException;

abstract class AbstractException extends BaseException
{
    /**
     * @var Click
     */
    public $click;

    /**
     * DuplicateClickException constructor.
     * @param $click $click
     * @param null $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($click, $message = null, $code = 0, \Exception $previous = null)
    {
        $this->click = $click;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Click Exception';
    }
}
