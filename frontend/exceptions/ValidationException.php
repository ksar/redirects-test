<?php
namespace frontend\exceptions;

class ValidationException extends AbstractException
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Invalid Click';
    }
}
