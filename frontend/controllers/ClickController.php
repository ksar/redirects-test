<?php
namespace frontend\controllers;

use yii\base\Module;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use frontend\models\Click;
use frontend\services\ClickServiceInterface;
use frontend\exceptions\AbstractException;
use frontend\exceptions\ValidationException;

class ClickController extends Controller
{

    /** @var ClickServiceInterface */
    protected $clickService;

    public function __construct($id, Module $module, ClickServiceInterface $clickService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->clickService = $clickService;
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @throws BadRequestHttpException
     */
    public function actionClick()
    {
        try {
            $click = $this->clickService->create(\Yii::$app->request);
            $this->redirect(Url::to(['success', 'id' => $click->id]));
        } catch (ValidationException $e) {
            throw new BadRequestHttpException($e->getMessage());
        } catch (AbstractException $e) {
            $this->redirect(Url::to(['error', 'id' => $e->click->id]));
        }
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSuccess($id)
    {
        $this->findModel($id); // check click by id
        return $this->render('success');
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionError($id)
    {
        $click = $this->findModel($id);
        if ($click->bad_domain) {
            return $this->render('domain-error');
        }
        return $this->render('error');
    }

    /**
     * @param $id
     * @return Click
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        if (!$click = $this->clickService->find($id)) {
            throw new NotFoundHttpException();
        }
        return $click;
    }
}
