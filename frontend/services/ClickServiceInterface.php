<?php
namespace frontend\services;

use yii\web\Request;
use frontend\models\Click;
use frontend\exceptions\ValidationException;
use frontend\exceptions\DuplicateClickException;
use frontend\exceptions\BadDomainException;

interface ClickServiceInterface
{
    /**
     * @param Request $request
     * @return Click
     * @throws ValidationException
     * @throws DuplicateClickException
     * @throws BadDomainException
     */
    public function create(Request $request);

    /**
     * @param string $id
     * @return Click|null
     */
    public function find($id);
}
