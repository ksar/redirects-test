<?php
namespace frontend\services;

use frontend\models\Click;
use frontend\models\query\ClickQuery;
use frontend\models\query\BadDomainQuery;
use frontend\exceptions\DuplicateClickException;
use frontend\exceptions\ValidationException;
use frontend\exceptions\BadDomainException;
use yii\web\Request;

class ClickService implements ClickServiceInterface
{
    /**
     * @var ClickQuery
     */
    public $clickQuery;
    /**
     * @var BadDomainQuery
     */
    public $domainQuery;

    /**
     * ClickService constructor.
     * @param ClickQuery $clickQuery
     * @param BadDomainQuery $domainQuery
     */
    public function __construct(ClickQuery $clickQuery, BadDomainQuery $domainQuery)
    {
        $this->clickQuery = $clickQuery;
        $this->domainQuery = $domainQuery;
    }

    /**
     * @inheritdoc
     */
    public function create(Request $request)
    {
        $ip = ip2long($request->getUserIP());
        $ua = $request->getUserAgent();
        $ref = $request->getReferrer();
        $param1 = $request->getQueryParam('param1');

        if ($click = $this->clickQuery->find($ip, $ua, $ref, $param1)) {
            $this->clickQuery->saveError($click);
            throw new DuplicateClickException($click);
        }

        $click = $this->clickQuery->createModel([
            'ip' => $ip,
            'ua' => $ua,
            'ref' => $ref,
        ] + $request->getQueryParams());
        if (!$click->save()) {
            throw new ValidationException($click);
        }
        if (!$this->isDomainValid($click)) {
            $click->bad_domain = true;
            $this->clickQuery->saveError($click);
            throw new BadDomainException($click);
        }
        return $click;
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return $this->clickQuery->findById($id);
    }

    /**
     * @param Click $click
     * @return bool
     */
    protected function isDomainValid(Click $click)
    {
        $domain = parse_url($click->ref, PHP_URL_HOST);
        return $domain && !$this->domainQuery->find($domain);
    }
}
