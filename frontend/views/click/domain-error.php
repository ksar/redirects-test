<?php

use yii\web\View;
use yii\bootstrap\Html;

/**
 * @var View $this
 */

$this->title = 'Bad Domain';

?>
<div class="site-error">
    <?=Html::tag('div', $this->title, ['class' => 'alert alert-danger'])?>
</div>
<script>
    setTimeout(function () {
        window.location = 'http://google.com';
    }, 5000);
</script>
