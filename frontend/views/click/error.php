<?php

use yii\web\View;
use yii\bootstrap\Html;

/**
 * @var View $this
 */

$this->title = 'Click Error';

?>
<div class="site-error">
    <?=Html::tag('div', $this->title, ['class' => 'alert alert-danger'])?>
</div>
