<?php

use yii\web\View;
use yii\bootstrap\Html;

/**
 * @var View $this
 */

$this->title = 'Click Success';

?>
<div class="site-error">
    <?=Html::tag('div', 'Success', ['class' => 'alert alert-success'])?>
</div>
