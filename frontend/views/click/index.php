<?php

use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 */

$this->title = 'Index';

?>
<div class="site-index">
    <?php ActiveForm::begin([
        'options' => ['csrf' => false],
        'action' => Url::to('/click/'),
        'method' => 'get',
    ]); ?>
    <?php for($i=1; $i<=2; $i++): ?>
        <div class="form-group">
            <?=Html::label('Param '.$i.':', 'param'.$i)?>
            <?=Html::textInput('param'.$i, 'value'.$i, ['id' => 'param'.$i])?>
        </div>
    <?php endfor; ?>
    <div class="form-group">
        <?=Html::button(Html::icon('link').' Test Link', [
            'class' => 'btn btn-default',
            'type' => 'submit',
        ])?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
