<?php

return [
    'id' => 'app-frontend',
    'name' => 'Redirects Test',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'click',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
        ],
        'urlManager' => [
            'class' => \yii\web\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' => [
                [
                    'pattern' => '<action:(click|success|error)>/<id>',
                    'route' => 'click/<action>',
                    'defaults' => ['id' => null],
                ],
            ],
        ],
    ],
];
