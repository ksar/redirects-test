<?php

\Yii::$container->set(
    \frontend\services\ClickServiceInterface::class,
    \frontend\services\ClickService::class
);

Yii::$container->set(
    \frontend\models\query\ClickQuery::class,
    \frontend\models\query\ClickQuery::class,
    [
        \frontend\models\Click::class
    ]
);

Yii::$container->set(
    \frontend\models\query\BadDomainQuery::class,
    \frontend\models\query\BadDomainQuery::class,
    [
        \common\models\BadDomain::class
    ]
);
