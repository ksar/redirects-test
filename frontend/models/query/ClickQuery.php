<?php
namespace frontend\models\query;

use yii\db\ActiveQuery;
use frontend\models\Click;

class ClickQuery extends ActiveQuery
{
    /**
     * @param array [$data]
     * @return Click
     */
    public function createModel(array $data = [])
    {
        $model = new Click();
        $model->load($data, '');
        return $model;
    }

    /**
     * @param string $id
     * @return Click|null
     */
    public function findById($id)
    {
        return Click::findOne($id);
    }

    /**
     * @param string $ip
     * @param string $ua
     * @param string $ref
     * @param string $param1
     * @return Click|null
     */
    public function find($ip, $ua, $ref, $param1)
    {
        return Click::find()
            ->where([
                'ip' => $ip,
                'ua' => $ua,
                'ref' => $ref,
                'param1' => $param1
            ])->one();
    }

    /**
     * @param Click $click
     */
    public function saveError(Click $click)
    {
        $click->error ++;
        $click->save();
    }
}
