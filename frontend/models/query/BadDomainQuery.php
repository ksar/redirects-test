<?php
namespace frontend\models\query;

use yii\db\ActiveQuery;
use common\models\BadDomain;

class BadDomainQuery extends ActiveQuery
{

    /**
     * @param string $name
     * @return BadDomain|null
     */
    public function find($name)
    {
        return BadDomain::find()
            ->where([
                'name' => $name,
            ])->one();
    }
}
