<?php
namespace frontend\models;

/**
 * @property string $cd
 */
class Click extends \common\models\Click
{
    public function rules()
    {
        return [
            [['ip', 'ua', 'ref'], 'required'],
            [['ip'], 'integer'],
            [['id', 'ua', 'ref', 'param1', 'param2'], 'string'],
            [['param1', 'param2'], 'default', 'value' => ''],
            [['id'], 'default', 'value' => function(){
                return uniqid();
            }],
            [['cd'], 'default', 'value' => function () {
                return time();
            }],
        ];
    }
}
