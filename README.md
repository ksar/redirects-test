# Redirects Test

Based on [Yii 2](http://www.yiiframework.com/) Advanced Project Template.

## Directory structure

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains backend widgets
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
tests/                   unit & functional tests
vendor/                  contains dependent 3rd-party packages
```

## Installation

### Development Environment

* run init instructions: `./init-container.sh`
* edit .env file manually (if needed)

### Development Urls
* http://localhost:8080/ - frontend with test link
* http://localhost:8080/admin/ - backend interface

### Default Credentials
* After docker deploy, backend credentials are: admin/123

## Environments
* Debug settings:
  * YII_DEBUG - (true|false)
  * YII_ENV - system environment (dev|prod)
* Request parameters
  * COOKIE_VALIDATION_KEY - random string, cookie salt
* Database settings
  * DB_DSN - database DSN (e.g. mysql:host=127.0.0.1;port=3306;dbname=redirects)
  * DB_USERNAME - database user name
  * DB_PASSWORD - database user password
  * DB_TEST_DSN - test database DSN (e.g. mysql:host=127.0.0.1;port=3306;dbname=redirects_test)
  * DB_TEST_USERNAME - test database user name
  * DB_TEST_PASSWORD - test database user password
  * DB_TABLE_PREFIX (optional)
* API settings
  * SITE_URL = http://localhost:8080/
  * SITE_TEST_URL = http://localhost:8081/

## Command line routines

* `console/yii migrate` - Upgrades the application by applying new migrations
* `vendor/bin/codecept run unit --config=tests/codeception` - Unit tests
