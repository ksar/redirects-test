<?php
namespace tests\codeception\api\unit\services;

use PHPUnit_Framework_MockObject_MockObject as MockObject;
use yii\codeception\TestCase;
use yii\web\Request;
use frontend\services\ClickService;
use frontend\services\ClickServiceInterface;
use frontend\models\Click;
use frontend\models\query\ClickQuery;
use frontend\models\query\BadDomainQuery;
use frontend\exceptions\BadDomainException;
use frontend\exceptions\DuplicateClickException;
use frontend\exceptions\ValidationException;

class ClickServiceTest extends TestCase
{
    /** @var Request | MockObject */
    protected $requestMock;

    /** @var ClickQuery | MockObject */
    protected $clickQueryMock;

    /** @var BadDomainQuery | MockObject */
    protected $badDomainQueryMock;

    /** @var ClickServiceInterface */
    protected $service;

    public function setUp()
    {
        $this->clickQueryMock = $this->getMockBuilder(ClickQuery::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->badDomainQueryMock = $this->getMockBuilder(BadDomainQuery::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new ClickService(
            $this->clickQueryMock,
            $this->badDomainQueryMock
        );
    }

    /**
     * @param $ip
     * @param $ua
     * @param $ref
     * @param $p1
     * @param $p2
     * @throws BadDomainException
     * @throws DuplicateClickException
     * @throws ValidationException
     * @dataProvider validCreateProvider
     */
    public function testCreate($ip, $ua, $ref, $p1, $p2)
    {
        $this->requestMock->expects($this->once())
            ->method('getUserIP')
            ->willReturn($ip);
        $this->requestMock->expects($this->once())
            ->method('getUserAgent')
            ->willReturn($ua);
        $this->requestMock->expects($this->once())
            ->method('getReferrer')
            ->willReturn($ref);
        $this->requestMock->expects($this->once())
            ->method('getQueryParam')
            ->with('param1')
            ->willReturn($p1);
        $this->requestMock->expects($this->once())
            ->method('getQueryParams')
            ->willReturn([
                'param1' => $p1,
                'param2' => $p2,
            ]);
        $clickMock = $this->createClickMock();
        $clickMock->expects($this->once())
            ->method('save')
            ->willReturn(true);
        $clickMock->expects($this->once())
            ->method('__get')
            ->with('ref')
            ->willReturn($ref);
        $this->clickQueryMock->expects($this->once())
            ->method('createModel')
            ->willReturn($clickMock);
        $this->service->create($this->requestMock);
    }

    /**
     * @return array
     */
    public function validCreateProvider()
    {
        return [
            [
                '8.8.8.8',
                'Some UserAgent',
                'http://some-referer/fdgfdgfdgfd',
                'param1',
                'param2',
            ],
            [
                '192.168.1.1',
                'Some UserAgent 333',
                'https://google.com/',
                'param1',
                null,
            ],
        ];
    }

    /**
     * @throws BadDomainException
     * @throws DuplicateClickException
     * @throws ValidationException
     * @expectedException \frontend\exceptions\DuplicateClickException
     */
    public function testCreateDuplicate()
    {
        $clickMock = $this->createClickMock();
        $this->clickQueryMock->expects($this->once())
            ->method('find')
            ->willReturn($clickMock);
        $this->service->create($this->requestMock);
    }

    /**
     * @throws BadDomainException
     * @throws DuplicateClickException
     * @throws ValidationException
     * @expectedException \frontend\exceptions\ValidationException
     */
    public function testCreateWithValidationFail()
    {
        $this->requestMock->expects($this->once())
            ->method('getQueryParams')
            ->willReturn([]);
        $clickMock = $this->createClickMock();
        $this->clickQueryMock->expects($this->once())
            ->method('createModel')
            ->willReturn($clickMock);
        $this->service->create($this->requestMock);
    }

    /**
     * @throws BadDomainException
     * @throws DuplicateClickException
     * @throws ValidationException
     * @expectedException \frontend\exceptions\BadDomainException
     */
    public function testCreateWithBadDomain()
    {
        $this->requestMock->expects($this->once())
            ->method('getQueryParams')
            ->willReturn([]);
        $clickMock = $this->createClickMock();
        $clickMock->expects($this->once())
            ->method('save')
            ->willReturn(true);
        $this->clickQueryMock->expects($this->once())
            ->method('createModel')
            ->willReturn($clickMock);
        $this->service->create($this->requestMock);
    }

    /**
     * @param $id
     * @param $click
     * @dataProvider findProvider
     */
    public function testFind($id, $click)
    {
        $this->clickQueryMock->expects($this->once())
            ->method('findById')
            ->with($id)
            ->willReturn($click);
        $this->assertEquals($click, $this->service->find($id));
    }

    /**
     * @return array
     */
    public function findProvider()
    {
        return [
            [
                111,
                $this->createClickMock(111),
            ],
        ];
    }

    /**
     * @param [$id]
     * @return MockObject|Click
     */
    public function createClickMock($id=null)
    {
        /** @var MockObject|Click $mock */
        $mock = $this->getMockBuilder(Click::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->id = $id;
        return $mock;
    }
}
