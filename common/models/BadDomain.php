<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * Bad domain model
 *
 * @property integer $id
 * @property string $name
 */
class BadDomain extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bad_domains}}';
    }
}
