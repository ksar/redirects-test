<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * Click model
 * @property integer $id
 * @property string $ua
 * @property int $ip
 * @property string $ref
 * @property string $param1
 * @property string $param2
 * @property int $error
 * @property bool $bad_domain
 * @property int $cd
 */
class Click extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%click}}';
    }
}
