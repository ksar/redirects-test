<?php

return [
    'id' => 'app-common-tests',
    'components' => [
        'db' => [
            'dsn' => getenv('DB_TEST_DSN'),
            'username' => getenv('DB_TEST_USERNAME'),
            'password' => getenv('DB_TEST_PASSWORD'),
            'tablePrefix' => getenv('DB_TEST_TABLE_PREFIX'),
        ],
    ],
];
