<?php
namespace console\migrations;

use yii\db\Migration;

class M180311232157ClickDate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%click}}', 'cd', $this->integer()->notNull());
        $this->createIndex('idx_click_date', '{{%click}}', ['cd']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%click}}', 'cd');
    }
}
