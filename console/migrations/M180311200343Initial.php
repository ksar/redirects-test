<?php
namespace console\migrations;

use yii\db\Migration;

class M180311200343Initial extends Migration
{
    const TABLE_OPTION = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%click}}', [
            'id' => $this->string(13)->notNull()->unique(),
            'ua' => $this->string(4000)->notNull(),
            'ip' => $this->integer()->unsigned()->notNull(),
            'ref' => $this->string()->notNull()->defaultValue(''),
            'param1' => $this->string()->notNull()->defaultValue(''),
            'param2' => $this->string()->notNull()->defaultValue(''),
            'error' => $this->integer()->notNull()->defaultValue(0),
            'bad_domain' => $this->boolean()->notNull()->defaultValue(0),
        ], self::TABLE_OPTION);
        $this->createIndex('idx_click', '{{%click}}', ['ua(32)', 'ip', 'ref', 'param1'], true);
        $this->createTable('{{%bad_domains}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
        ], self::TABLE_OPTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%click}}');
        $this->dropTable('{{%bad_domains}}');
    }
}
