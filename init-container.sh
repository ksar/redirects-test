#!/bin/bash

MYSQL_START_DELAY=10

docker-compose up -d
docker-compose exec cli composer install

if [ ! -f .env ]; then
    cp .env.dist .env
fi

echo "Waiting ${MYSQL_START_DELAY} seconds for mysql daemon starting..."
sleep ${MYSQL_START_DELAY}
docker-compose exec cli console/yii app/setup --interactive=0

# functional tests environments
#docker-compose exec cli php tests/codeception/yii migrate --interactive=0
#vendor/bin/codecept run functional --config=tests/codeception
