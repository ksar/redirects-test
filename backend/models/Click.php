<?php
namespace backend\models;

/**
 * @property string $cd
 */
class Click extends \common\models\Click
{
    /**
     * @return string
     */
    public function getDecodedDate()
    {
        return date('Y-m-d H:i', $this->cd);
    }

    /**
     * @return string
     */
    public function getDecodedIp()
    {
        return long2ip($this->ip);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ua' => 'User Agent',
            'ip' => 'IP address',
            'decodedIp' => 'IP address',
            'ref' => 'HTTP Referer',
            'param1' => 'Parameter #1',
            'param2' => 'Parameter #2',
            'error' => 'Errors count',
            'bad_domain' => 'Bad domain',
            'cd' => 'Click Time',
            'decodedDate' => 'Click Time',
        ];
    }
}
