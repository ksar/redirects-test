<?php
namespace backend\models;

class BadDomain extends \common\models\BadDomain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'unique'],
        ];
    }
}
