<?php

use yii\bootstrap\Html;
use yii\web\View;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use backend\models\Click;

/**
 * @var View $this
 * @var Click $model
 * @var ActiveDataProvider $membershipDataProvider
 * @var ActiveDataProvider $logsDataProvider
 * @var array $itemTypes
 */
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clicks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-view">
   <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'decodedDate',
            'ua',
            'decodedIp',
            'ref',
            'param1',
            'param2',
            [
                'attribute' => 'bad_domain',
                'value' => $model->bad_domain ? Html::icon('ok') : '',
                'format' => 'raw',
            ],
            'error',
        ],
    ]) ?>
</div>
