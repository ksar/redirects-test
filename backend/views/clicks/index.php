<?php

use yii\web\View;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use backend\models\Click;
use backend\components\grid\ActionColumn;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Clicks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'decodedDate',
            'decodedIp',
            [
                'attribute' => 'bad_domain',
                'value' => function(Click $model){
                    return $model->bad_domain ? Html::icon('ok') : '';
                },
                'format' => 'raw',
            ],
            'error',
            [
                'class' => ActionColumn::class,
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
