<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Dictionaries', 'options' => ['class' => 'header']],
                    ['label' => 'Clicks', 'icon' => 'link', 'url' => ['/clicks']],
                    ['label' => 'Bad Domains', 'icon' => 'fire', 'url' => ['/bad-domains']],
                    ['label' => 'System', 'options' => ['class' => 'header']],
                    ['label' => 'Users', 'icon' => 'users', 'url' => ['/users']],
                ],
            ]
        ) ?>

    </section>

</aside>
