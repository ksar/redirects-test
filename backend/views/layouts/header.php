<?php

use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Html;
use backend\models\User;
use backend\widgets\Button;

/**
 * @var View $this
 * @var User $user
 * @var string $directoryAsset
 * @var string $content
 */
$user = Yii::$app->user->identity;

?>

<header class="main-header">

    <a class="logo" href="<?=Url::to(Yii::$app->homeUrl)?>">
        <span class="logo-mini"><?=Html::icon('link')?></span>
        <span class="logo-lg"><?=Html::icon('link')?> <?=Yii::$app->name?></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user-md"></i>
                        <span class="hidden-xs"><?= $user->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Button::widget([
                                    'label' => 'Profile',
                                    'link' => ['/managers/view', 'id' => $user->id],
                                ]) ?>
                            </div>
                            <div class="pull-right">
                                <?= Button::widget([
                                    'label' => 'Sign out',
                                    'link' => ['/site/logout'],
                                    'options' => ['data-method' => 'post'],
                                ]) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
