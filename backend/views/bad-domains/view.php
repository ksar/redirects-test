<?php

use yii\web\View;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use backend\widgets\ButtonEdit;
use backend\widgets\ButtonDelete;
use backend\models\BadDomain;

/**
 * @var View $this
 * @var BadDomain $model
 * @var ActiveDataProvider $membershipDataProvider
 * @var ActiveDataProvider $logsDataProvider
 * @var array $itemTypes
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bad Domains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-view">
    <p>
        <?=ButtonEdit::widget([
            'link' => ['update', 'id' => $model->id],
        ])?>
        <?=ButtonDelete::widget([
            'link' => ['delete', 'id' => $model->id],
        ])?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>
</div>
