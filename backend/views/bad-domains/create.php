<?php

use yii\web\View;
use backend\models\BadDomain;

/**
 * @var View $this
 * @var BadDomain $model
 */

$this->title = 'Add Domain';
$this->params['breadcrumbs'][] = ['label' => 'Bad Domains', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Add';
?>
<div class="mailer-domain-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
