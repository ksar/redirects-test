<?php

use yii\web\View;
use backend\models\BadDomain;

/**
 * @var View $this
 * @var BadDomain $model
 */
$this->title = 'Update User: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bad Domains', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="document-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
