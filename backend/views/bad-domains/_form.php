<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use backend\models\BadDomain;
use backend\widgets\ButtonSave;

/**
 * @var View $this
 * @var BadDomain $model
 * @var ActiveForm $form
 */
?>
<div class="mailer-domain-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?=ButtonSave::widget()?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
