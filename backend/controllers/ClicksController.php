<?php
namespace backend\controllers;

use backend\models\Click;
use backend\components\Controller;
use yii\data\ActiveDataProvider;

/**
 * BadDomainsController implements the CRUD actions for Click model.
 * @method Click findModel($id)
 */
class ClicksController extends Controller
{
    /**
     * @var string
     */
    protected $modelClass = Click::class;

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'sort' => false,
            'query' => Click::find(),
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
}
