<?php
namespace backend\controllers;

use Yii;
use backend\models\BadDomain;
use backend\components\Controller;
use yii\data\ActiveDataProvider;

/**
 * BadDomainsController implements the CRUD actions for BadDomain model.
 * @method BadDomain findModel($id)
 */
class BadDomainsController extends Controller
{
    /**
     * @var string
     */
    protected $modelClass = BadDomain::class;

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BadDomain::find(),
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BadDomain();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this
                ->addFlashMessage('Domain added: ' . $model->name, self::FLASH_SUCCESS)
                ->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this
                ->addFlashMessage('Domain updated: ' . $model->name, self::FLASH_SUCCESS)
                ->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            $this->addFlashMessage('Domain deleted: ' . $model->name, self::FLASH_WARNING);
        }
        return $this->redirect(['index']);
    }
}
